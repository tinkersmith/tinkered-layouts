<?php

namespace Drupal\tinkered_layout\Annotation;

use Drupal\Core\Annotation\Plugin;

/**
 * Annotation to mark layout snippets that contains partial layouts.
 *
 * @see plugin_api
 *
 * @ingroup tinkered_layout_snippets
 *
 * @Annotation
 */
class LayoutSnippet extends Plugin {

  /**
   * Unique identifier for this layout snippet.
   *
   * @var \string
   */
  public $id;

  /**
   * Human friendly display name.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $name;

  /**
   * Machine name of the module that provides this snippet.
   *
   * @var \string
   */
  public $module;

  /**
   * Information required to build layout Sass (includes path and placeholders).
   *
   * @var object
   */
  public $sassData;

  public $iconFile;

  /**
   * Array listing the regions provided by this snippet.
   *
   * @var array
   */
  public $regions;
}
