<?php

namespace Drupal\tinkered_layout\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 *
 */
class TinkeredLayoutForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $state) {
    $form = parent::form($form, $state);
    $layout = $this->entity;

    if ($this->operation === 'add') {
      $form['#title'] = $this->t('Create New Layout');
    }
    elseif ($this->operation === 'edit') {
      $form['#title'] = $this->t('Edit %title Layout', ['%title' => $layout->label()]);
    }

    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Layout name'),
      '#default_value' => $layout->label(),
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#label' => $this->t('Machine name'),
      '#default_value' => $layout->id(),
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
      '#disabled' => !$layout->isNew(),
      '#machine_name' => [
        'source' => ['name'],
        'exists' => ['Drupal\tinkered_layout\Entity\TinkeredLayout', 'load'],
      ],
    ];

    // Determine that theme that this layout is created for.
    $themes = system_list('theme');
    $themeOpts = array();
    foreach ($themes as $theme => $themeInfo) {
      $themeOpts[$theme] = $themeInfo['name'];
    }

    $form['theme'] = [
      '#type' => 'select',
      '#title' => $this->t('Theme'),
      '#options' => $themeOpts,
    ];

    // Interactive building of snippet sets.
    $form['snippets'] = [ ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $state) {
    $layout = $this->entity;
    $layout->save();
  }

}
