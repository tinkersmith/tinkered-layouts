<?php

namespace Drupal\tinkered_layout\Entity;

use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Default implementation of the TinkeredLayout configuration class.
 *
 * @ConfigEntityType(
 *   id = "tinkered_layout",
 *   label = @Translation("Tinkered Layout"),
 *   fieldable = FALSE,
 *   controllers = {
 *     "list_builder" = "Drupal\tinkered_layout\LayoutListBuilder",
 *     "form" = {
 *       "rebuild" => "Drupal\tinkered_layout\Form\TinkeredLayoutRebuildForm",
 *       "add" = "Drupal\tinkered_layout\Form\TinkeredLayoutForm",
 *       "edit" = "Drupal\tinkered_layout\Form\TinkeredLayoutForm",
 *       "delete" = "Drupal\tinkered_layout\Form\TinkeredLayoutDeleteForm"
 *     }
 *   },
 *   config_prefix = "tinkered_layout",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "uuid",
 *     "label" = "name",
 *     "weight" = "weight"
 *   },
 *   links = {
 *     "rebuild" = "entity.tinkered_layout.canonical",
 *     "edit-form" = "entity.tinkered_layout.edit",
 *     "delete-form" = "entity.tinkered_layout.delete"
 *   },
 *   config_export = {
 *     "uuid",
 *     "name",
 *     "category",
 *     "desc",
 *     "theme",
 *     "snippets"
 *   }
 * )
 */
class TinkeredLayout extends ConfigEntityBase implements TinkeredLayoutInterface {

  /**
   * A Universally unique identifier for this layout.
   *
   * Layouts are likely to be shared and exportable to different environments
   * and thus need to be uniquely identified. A stored hash of layout state
   * will also be used to compare the layout's matching state.
   *
   * @var  \Drupal\Component\Uuid\UuidInterface
   */
  private $uuid;

  /**
   * The human friendly display name.
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  private $name;

  /**
   * A description of the layout.
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  private $desc;

  /**
   * A grouping name to use for the layout (e.g. "page", "content", etc...).
   *
   * @var \string
   */
  private $category;

  /**
   * The name of the theme this layout is associated with.
   *
   * Layouts are associated to the theme they are generated for. The reason
   * for this is that there are theme specific parameters that layouts will
   * want to be configured for, like breakpoints. Some layouts can be agnostic
   * of the theme, if this property is left as NULL.
   *
   * @var \string
   */
  private $theme;

  /**
   * An array of LayoutSnippetSet objects, which define the final layout.
   *
   * @var \Druapl\tinkered_layout\Plugin\LayoutSnippetSet[]
   */
  protected $snippets;

  /**
   * {@inheritdoc}
   */
  public function toRenderable() {

  }

}
