<?php

namespace Drupal\tinkered_layout\Entity;

use Drupal\Core\Render\RenderableInterface;
use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * The base interface required for a TinkeredLayout configuration.
 *
 * TinkeredLayouts should be expected to contain the layout definition needed
 * to create a Layout Renderer, compile the CSS and build the Twig templates.
 * It should, however, only contain the definition and not be expected to
 * generate the derivative classes.
 *
 * The actual rendering should be handled by the layout renderer.
 */
interface TinkeredLayoutInterface extends ConfigEntityInterface, RenderableInterface {

}
